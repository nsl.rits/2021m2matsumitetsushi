READMEファイル

はじめに：

シナリオ内で指定しているモビリティファイル(.wkt)は、
研究室内部向けの研究成果ファイル内にあるため、そちらから取得してください。


c言語のファイルであるhiker.cは、私がシミュレーションシナリオ内の、
登山者の出発タイミングの入力を簡略化するために作成したファイルです。



参考文献：

シミュレータ「The ONE」公式サイト
https://www.netlab.tkk.fi/tutkimus/dtn/theone/

シミュレータ「The ONE」の文献
A. Keranen, J. Ott, and T. Karkkainen,
"The ONE Simulator for DTN Protocol Evaluation",
SIMUTools’09: 2nd International Conference on Simulation Tools and Techniques, pp55:1-55:10,2009.
